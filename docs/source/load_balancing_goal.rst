.. _load_balancing_goal:

Load-balancing goal for bandwidth scanners
==========================================

teor’s initial draft:

.. parsed-literal::

   Bandwidth scanners should have a well-defined network equilibrium goal.

   For sbws and Torflow without PID control, the network equilibrium goals are:
     1. clients experience reasonably consistent performance
     2. clients experience performance that is as fast as possible, without compromising goal 1
   These performance goals include both throughput and latency.

   For Torflow with PID control, the goal was:
     1. Each relay has exactly the same spare capacity for an additional stream
   This goal was unachievable because Tor did not provide enough feedback on circuit failure.

   These non-goals are common suggestions:
     1. Bandwidth is allocated equally across relays
     2. All relay bandwidth is used
   These goals are unachievable, because they conflict with the consistent client performance goal.

Based on Mike’s response from a `tor-dev
thread <https://lists.torproject.org/pipermail/tor-dev/2018-August/013419.html>`__:

.. parsed-literal::

   I believe quite strongly that even if the Tor network gets faster on
   average, if this comes at the cost of increased performance variance,
   user experience and perceived speed of Tor will be much worse. There's
   nothing more annoying than a system that is *usually* fast enough to do
   what you need it to do, but fails to be fast enough for that activity at
   unpredictable times.

For metrics relevant for load balancing evaluation, see: `Balancing
metrics <https://gitlab.torproject.org/tpo/network-health/team/-/wikis/Tor-network-health-work#balancing-metrics>`__.
