.. _index:

Bandwidth in Tor
================

This repository contains documentation about bandwidth scanners and values in
the Tor network and previous work.

.. toctree::
   :maxdepth: 1
   :caption: Documentation about Tor bandwidth and scanners:
   :numbered:

   introduction
   bandwidth_values
   bandwidth_tor
   bandwidth_tor_code
   torflow_aggr
   current_bandwidth_scanners/index
   bandwidth_authority_measurements
   load_balancing_goal
   glossary
   references

* :ref:`genindex`

.. toctree::
   :maxdepth: 1
   :caption: Extra documentation

   old_bandwidth_scanners/index
   past_work/index
   useful_doc/index

External documentation
----------------------

Old bandwidth authority specifications:

- [TorflowPaper]_
- [TORFLOWSPEC]_
- [TorflowBwAuthorities]_
- [TorflowREADME]_

Current specification:

- [BandwidthFileSpec]_

Performance:

- [PerformanceMetrics]_

Slides:

- [Bornhack2018]_
